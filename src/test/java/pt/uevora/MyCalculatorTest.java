package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    @Test
    public void testSum() throws Exception {
        Double result = calculator.execute("2+3");

        assertEquals("The sum result of 2 + 3 must be 5",  5.0, (Object)result);
    }

    @Test
    public void testSub() throws Exception {
        Double result = calculator.execute("2-3");

        assertEquals("The sum result of 2 - 3 must be -1",  -1.0, (Object)result);
    }

    @Test
    public void testMult() throws Exception {
        Double result = calculator.execute("2*3");

        assertEquals("The sum result of 2 - 3 must be 6",  6.0, (Object)result);
    }

    @Test
    public void testDiv() throws Exception {
        Double result = calculator.execute("5/2");

        assertEquals("The sum result of 5 / 2 must be 2.5",  2.5, (Object)result);
    }

    @Test
    public void testPow() throws Exception {
        Double result = calculator.execute("2^10");

        assertEquals("The sum result of 2^10 must be 1024",  1024.0, (Object)result);
    }
}